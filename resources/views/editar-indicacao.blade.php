@extends('layouts.app')
@section('content')
<div class="container">
<div class="row justify-content-center">
   <div class="col-md-8">
      <div class="card">
         <div class="card-header">{{ __('Insira os dados do Imóvel') }}</div>
         <div class="card-body">
            <form method="POST" action="{{ route('atualizar-indicacao') }}">
               @csrf
               <div class="form-group row">
                  <label for="nome" class="col-md-4 col-form-label text-md-right">{{ __('Nome do propretário') }}</label>

                  <div class="col-md-6">
                  <input id="nome" type="text" value="{{$proprietario->nome}}" class="form-control @error('nome') is-invalid @enderror" name="nome" value="{{ old('nome') }}" required autocomplete="name" autofocus>
                  <input id="id_prop" type="text" hidden  name="id_prop" value="{{$indicacao[0]->id_proprietario}}" required autocomplete="name" >
                  <input id="id_captacao" type="text" hidden name="id_captacao" value="{{$indicacao[0]->id_captacao}}" required autocomplete="name" >
                  <input id="id_indicacao" type="text" hidden name="id_indicacao" value="{{$indicacao[0]->id}}"  autocomplete="name" >

                      @error('nome')
                      <span class="invalid-feedback" role="alert">
                          <strong>{{ $message }}</strong>
                      </span>
                      @enderror
                  </div>
              </div>

              <div class="form-group row">
                  <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail') }}</label>

                  <div class="col-md-6">
                      <input id="email" type="email" value="{{$proprietario->email}}" class="form-control  @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                      @error('email')
                      <span class="invalid-feedback" role="alert">
                          <strong>{{ $message }}</strong>
                      </span>
                      @enderror
                  </div>
              </div>

              <div class="form-group row">
                  <label for="celular" class="col-md-4 col-form-label text-md-right">{{ __('Celular') }}</label>
                  <div class="col-md-6">
                      <input id="celular" type="text" value="{{$proprietario->telefone}}" class="form-control @error('celular') is-invalid @enderror" name="celular" required>
                      @error('celular')
                      <span class="invalid-feedback" role="alert">
                          <strong>{{ $message }}</strong>
                      </span>
                      @enderror
                  </div>
              </div>
               <div class="form-group row">
                  <label for="finalidade" class="col-md-4 col-form-label text-md-right">{{ __('Finalidade') }}</label>
                  <div class="col-md-6">
                  <select class="custom-select" id="finalidade" name="finalidade">
                        @if ($captacao->finalidade == "V")
                           <option  selected value="V">Venda</option>
                           <option  value="L">Locação</option>
                        @else
                           <option  value="V">Venda</option>
                           <option  selected value="L">Locação</option>
                        @endif
                     </select>
                  </div>
               </div>
               <div class="form-group row">
                  <label for="tipo" class="col-md-4 col-form-label text-md-right">{{ __('Tipo do Imóvel') }}</label>
                  <div class="col-md-6">
                  <input id="tipo" value="{{$captacao->tipo}}" type="text" class="form-control @error('tipo') is-invalid @enderror" name="tipo" required>
                     @error('tipo')
                     <span class="invalid-feedback" role="alert">
                     <strong>{{ $message }}</strong>
                     </span>
                     @enderror
                  </div>
               </div>
               <div class="form-group row">
                  <label for="cepImovel" class="col-md-4 col-form-label text-md-right">{{ __('CEP') }}</label>
                  <div class="col-md-6">
                  <input id="cepImovel" type="text" value="{{$captacao->cep}}" class="form-control @error('cep') is-invalid @enderror" name="cep" required autocomplete="new-cep">
                     @error('cep')
                     <span class="invalid-feedback" role="alert">
                     <strong>{{ $message }}</strong>
                     </span>
                     @enderror
                  </div>
               </div>
               <div class="form-group row">
                  <label for="enderecoImovel" class="col-md-4 col-form-label text-md-right">{{ __('Endereço') }}</label>
                  <div class="col-md-6">
                     <input id="enderecoImovel" type="text"  value="{{$captacao->endereco}}" class="form-control @error('endereco') is-invalid @enderror" name="endereco" required autocomplete="new-endereco">
                     @error('endereco')
                     <span class="invalid-feedback" role="alert">
                     <strong>{{ $message }}</strong>
                     </span>
                     @enderror
                  </div>
               </div>
               <div class="form-group row">
                  <label for="bairroImovel" class="col-md-4 col-form-label text-md-right">{{ __('Bairro') }}</label>
                  <div class="col-md-6">
                     <input id="bairroImovel" type="text"  value="{{$captacao->bairro}}" class="form-control @error('bairro') is-invalid @enderror" name="bairro" required autocomplete="new-bairro">
                     @error('bairro')
                     <span class="invalid-feedback" role="alert">
                     <strong>{{ $message }}</strong>
                     </span>
                     @enderror
                  </div>
               </div>
               <div class="form-group row">
                  <label for="numeroEnderecoImovel" class="col-md-4 col-form-label text-md-right">{{ __('Número da Residência') }}</label>
                  <div class="col-md-6">
                     <input id="numeroEnderecoImovel" type="text"  value="{{$captacao->numero}}" class="form-control" name="numero" required autocomplete="new-numero">
                  </div>
               </div>
               <div class="form-group row">
                  <label for="cidadeImovel" class="col-md-4 col-form-label text-md-right">{{ __('Cidade') }}</label>
                  <div class="col-md-6">
                     <input id="cidadeImovel" type="text"  value="{{$captacao->cidade}}" class="form-control @error('cidade') is-invalid @enderror" name="cidade" required>
                     @error('cidade')
                     <span class="invalid-feedback" role="alert">
                     <strong>{{ $message }}</strong>
                     </span>
                     @enderror
                  </div>
               </div>
               <div class="form-group row">
                  <label for="estadoImovel" class="col-md-4 col-form-label text-md-right">{{ __('Estado') }}</label>
                  <div class="col-md-6">
                     <input id="estadoImovel" type="text"  value="{{$captacao->estado}}" class="form-control @error('estado') is-invalid @enderror" name="estado" required>
                     @error('estado')
                     <span class="invalid-feedback" role="alert">
                     <strong>{{ $message }}</strong>
                     </span>
                     @enderror
                  </div>
               </div>
               <div class="form-group row">
                  <label for="complemento" class="col-md-4 col-form-label text-md-right">{{ __('Complemento') }}</label>
                  <div class="col-md-6">
                     <input id="complemento" type="text"  value="{{$captacao->complemento}}"class="form-control @error('complemento') is-invalid @enderror" name="complemento" required>
                     @error('complemento')
                     <span class="invalid-feedback" role="alert">
                     <strong>{{ $message }}</strong>
                     </span>
                     @enderror
                  </div>
               </div>
               <div class="form-group row mb-0">
                  <div class="col-md-6 offset-md-4">
                     <button type="submit" id="submit" class="btn btn-primary">
                     {{ __('Atualizar') }}
                     </button>
                  </div>
               </div>
            </form>
         </div>
      </div>
   </div>
</div>
@endsection