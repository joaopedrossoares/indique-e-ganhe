@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">Minhas Bonificações</div>
                <div class="card-body">
                    @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                    @endif

                    <table class="table">
                        <thead>
                            <tr>
                                <th scope="col">Nome do Proprietário</th>
                                <th scope="col">Endereço do Imóvel</th>
                                <th scope="col">Status</th>
                                <th scope="col">Data da disponibilização</th>
                                <th scope="col">Data da último depósito</th>
                                <th scope="col">Ações</th>
                            </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection