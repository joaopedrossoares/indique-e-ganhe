@extends('layouts.app')
@section('content')
<div class="container">
   <div class="row justify-content-center">
      <div class="col-md-12">
         <div class="card">
            <a href="/indique" class="btn btn-primary">Quero Indicar!</a>
            <div class="card-header">Minhas Indicações</div>
            <div class="card-body">
               @if (session('status'))
               <div class="alert alert-success" role="alert">
                  {{ session('status') }}
               </div>
               @endif
               <table class="table">
                  <thead>
                     <tr>
                        <th scope="col">Nome do Proprietário</th>
                        <th scope="col">Endereço do Imóvel</th>
                        <th scope="col">Status</th>
                        <th scope="col">Data da indicação</th>
                        <th scope="col">Data da última atualização</th>
                        <th scope="col">Ações</th>
                     </tr>
                  </thead>
                  <tbody>
                     @forelse ($indicacoes as $indicacao)
                     <tr>
                        <td>{{$indicacao->prop_nome}}</td>
                        <td>{{$indicacao->endereco_imovel}}</td>
                        @switch($indicacao->status)
                        @case(1)
                            <td>Aguardando contato</td>
                            @break
                        @case(2)
                            <td> Contato Feito </td>
                            @break
                        @case(3)
                            <td> Captação Feita </td>
                            @break
                        @case(4)
                            <td> Captação Rejeitada </td>
                            @break
                        @case(5)
                            <td> Captação Cancelada </td>
                            @break
                        @default
                        @endswitch
                        <td>{{$indicacao->created_at}}</td>
                        <td>{{$indicacao->updated_at}}</td>
                        <td > <a class="btn btn-primary pull-right" href="{{route("editar-indicacao", $indicacao->id)}}" >Ações</a></td>
                     </tr>
                     @empty
                     <p> Não há dados </p>
                     @endforelse
                  </tbody>
               </table>
            </div>
         </div>
      </div>
   </div>
</div>
@endsection