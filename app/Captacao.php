<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
// use Nicolaslopezj\Searchable\SearchableTrait;

class Captacao extends Model {

    protected $table = 'captacao_imoveis';
    protected $fillable = [
        'corretor_id',
        'nome',
        'finalidade',
        'tipo',
        'cep',
        'endereco',
        'numero',
        'bairro',
        'cidade',
        'estado',
        'complemento',
    ];

    const VENDA     = 'V';
    const LOCACAO   = 'L';
    const PLANTA    = 'PL';
    const CONTRUCAO = 'CO';
    const PRONTO    = 'PR';
}