<?php 
namespace App\Enums;

class StatusIndicacao {
    const AGUARDANDO_CONTATO = 1;
    const CONTATO_FEITO = 2;
    const CAPTACAO_FEITA = 3;
    const CAPTACAO_REJEITADA =4;
    const CAPTACAO_CANCELADA = 5;
}