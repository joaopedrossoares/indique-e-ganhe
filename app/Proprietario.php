<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Proprietario extends Model
{

    CONST TIPO_VENDA         = 1;
    CONST TIPO_ALUGUEL       = 2;
    CONST TIPO_VENDA_ALUGUEL = 3;

    CONST TIPOS_VENDAS = [
        1 => "Venda",
        2 => "Aluguel",
        3 => "Venda e Aluguel",
    ];
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nome',
        'telefone',
        'email',
    ];

    
}