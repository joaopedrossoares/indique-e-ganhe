<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Captacao;
use App\Proprietario;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Indicacao;
use App\Enums\StatusIndicacao;

class RegistroIndicacaoController extends Controller
{
        /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function store(Request $request)
    {
        $idProprietario = $this->saveProprietario($request);
        $idCaptacao = $this->saveCaptacao($request);
        $this->saveIndicacao($idCaptacao, $idProprietario);
        return HomeController::index();
    }

    private function saveProprietario(Request $request){
        $proprietario = new Proprietario();
        $proprietario->nome = $request->input('nome');
        $proprietario->telefone = $request->input('celular');
        $proprietario->email = $request->input('email');
        $proprietario->save();

        return $proprietario->id;
    }

    private function saveCaptacao(Request $request) {
        $imovel = new Captacao();
        $imovel->corretor_id = Auth::user()->id;
        $imovel->nome = $request->input('nome');
        $imovel->finalidade = $request->input('finalidade');
        $imovel->tipo = $request->input('tipo');
        $imovel->cep = $request->input('cep');
        $imovel->endereco = $request->input('endereco');
        $imovel->numero = $request->input('numero');
        $imovel->bairro = $request->input('bairro');
        $imovel->cidade = $request->input('cidade');
        $imovel->estado = $request->input('estado');
        $imovel->complemento = $request->input('complemento');
        $imovel->save();

        return $imovel->id;
    }

    private function saveIndicacao($idCaptacao, $idProprietario) {
        $indicacao = new Indicacao();
        $indicacao->status = StatusIndicacao::AGUARDANDO_CONTATO;
        $indicacao->id_usuario = Auth::user()->id;
        $indicacao->id_captacao = $idCaptacao;
        $indicacao->id_proprietario = $idProprietario;
        $indicacao->save();
        
        return $indicacao->id;
    }

    public function update(Request $request){
        $idProp = $request->id_prop;
        $idImovel = $request->id_captacao;

        $proprietario = Proprietario::find($idProp);
        $proprietario->nome = $request->input('nome');
        $proprietario->telefone = $request->input('celular');
        $proprietario->email = $request->input('email'); 
        $proprietario->save();

        $imovel = Captacao::find($idImovel);
        $imovel->nome = $request->input('nome');
        $imovel->finalidade = $request->input('finalidade');
        $imovel->tipo = $request->input('tipo');
        $imovel->cep = $request->input('cep');
        $imovel->endereco = $request->input('endereco');
        $imovel->numero = $request->input('numero');
        $imovel->bairro = $request->input('bairro');
        $imovel->cidade = $request->input('cidade');
        $imovel->estado = $request->input('estado');
        $imovel->complemento = $request->input('complemento');
        $imovel->save();  
        return HomeController::index();
    }

    public function paginaEdicao(Request $request){
        $id = $request->id;
        $indicacao = Indicacao::select('id_captacao', 'id_proprietario')->where('id', $id)->get();
        $idProprietario = $indicacao[0]->id_proprietario;
        $idCaptacao = $indicacao[0]->id_captacao;
        $captacao = Captacao::find($idCaptacao);
        $proprietario = Proprietario::find($idProprietario);
        // var_dump($captacao);die;
        return view("editar-indicacao", compact('captacao', 'proprietario','indicacao'));
    }

}
