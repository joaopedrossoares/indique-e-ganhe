<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use function GuzzleHttp\json_encode;
use App\UserIndiqueEGanhe;

class EditarUsuarioController extends Controller
{
    public function editar(){
        $dados = Auth::user();
        return view('editarUsuario', compact('dados'));
    }

    public function atualizar(Request $request) {
        $usuario = UserIndiqueEGanhe::find(Auth::user()->id);
        $usuario->update($request->all());    
        return HomeController::index();
    }

    private function getDadosAtuais() {
        $user = Auth::user();
        
        return [
            "nome" => $user->name,
            "email" => $user->email,
            "cpf" => $user->cpf,
            "telefone" => $user->phone

        ];
    }
}
