<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use function GuzzleHttp\json_encode;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public static function index()
    {
            $indicacoes = self::getDadosIndicacao();
            return view('home', compact('indicacoes'));
    }

    private static function getDadosIndicacao(){
        $id = Auth::user()->id;
        
        $indicacao = DB::select(DB::raw("
            SELECT i.id, i.status, i.created_at, i.updated_at, p.nome as prop_nome, p.endereco as prop_endereco, c.endereco as endereco_imovel 
            FROM indicacao i 
            JOIN captacao_imoveis c
            ON c.id = i.id_captacao
            JOIN proprietarios p 
            ON i.id_proprietario = p.id WHERE i.id = {$id} ;
        "));

        return $indicacao;
    }
}
