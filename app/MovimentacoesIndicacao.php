<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MovimentacoesIndicacao extends Model
{
    protected $table = 'movimentacoes_indicacao';

    protected $fillable = [
        'status',
        'id_indicacao'
    ];

}
