<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class NovoTipoUsuario extends Seeder
{
      /**
         * Rodar essa query no banco.
         * 
         * INSERT INTO imovesys.tipos_usuarios
         * (titulo, created_at, updated_at)
         * VALUES('Indicador de imóveis', null, null);
         */
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tipos_usuarios')->insert([
            'titulo' => "Indicador de imóveis"
        ]);
    }
}
