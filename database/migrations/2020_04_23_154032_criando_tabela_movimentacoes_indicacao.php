<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CriandoTabelaMovimentacoesIndicacao extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('movimentacoes_indicacao', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->enum('status',[1, 2, 3]);
            $table->bigInteger('id_indicacao')->unsigned();

            $table->foreign('id_indicacao')->on('indicacao')->references('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('movimentacoes_indicacao');
    }
}
