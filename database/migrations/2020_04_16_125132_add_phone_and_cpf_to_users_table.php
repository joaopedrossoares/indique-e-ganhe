<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddPhoneAndCpfToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users_indique_e_ganhe', function (Blueprint $table) {
            $table->bigInteger('phone');
            $table->string('cpf', 11);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users_indique_e_ganhe', function (Blueprint $table) {
            $table->dropColumn('phone');
            $table->dropColumn('cpf');
        });
    }
}
