<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CriandoTabelaIndicacao extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('indicacao', function (Blueprint $table) {
            $table->id();
            $table->enum('status',[1, 2, 3, 4, 5]);
            $table->bigInteger('id_usuario')->unsigned();
            $table->unsignedInteger('id_proprietario')->nullable();
            $table->integer('id_captacao')->unsigned()->nullable();
            $table->dateTime('created_at');	
            $table->dateTime('updated_at')->nullable();	

           $table->foreign('id_usuario')
            ->references('id')->on('users_indique_e_ganhe')
            ->onDelete('cascade');

            $table->foreign('id_captacao')
            ->references('id')->on('captacao_imoveis')
            ->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('indicacao');
    }
}