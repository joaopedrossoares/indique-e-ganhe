<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CriandoTabelaConfiguracaoIndiqueEGanhe extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('configuracao_indique_e_ganhe', function (Blueprint $table) {
            $table->id();
            $table->double('valor_indicacao')->nullable();
            $table->double('percent_primeiro_aluguel')->nullable();
            $table->integer('quantidade_dias_pos_solicitacao')->nullable();
            $table->bigInteger('id_indicacao')->unsigned()->nullable();
            $table->dateTime('created_at');	
            $table->dateTime('updated_at')->nullable();	

            $table->foreign('id_indicacao')->on('indicacao')->references('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('configuracao_indique_e_ganhe');
    }
}
