<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\RegistroIndicacaoController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    HomeController::index();
});

Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/indique', 'IndicacaoController@index')->name('indicacao');
Route::post('/registro-indicacao', 'RegistroIndicacaoController@store')->name('registro-indicacao');

Route::group(['prefix' => 'editar-indicacao'], function () {
    Route::get('/{id}', 'RegistroIndicacaoController@paginaEdicao')->name('editar-indicacao');
    Route::post("/atualiza", 'RegistroIndicacaoController@update')->name('atualizar-indicacao');
});

Route::group(['prefix' => 'editar-usuario'], function () {
    Route::post('editar','EditarUsuarioController@editar')->name('editar');
    Route::post('atualizar', 'EditarUsuarioController@atualizar')->name('atualizar');
});

Route::group(['prefix' => 'minhas-bonificacoes'], function () {
    Route::post('/','MovimentacoesIndicacaoController@index')->name('editar');
    Route::post('atualizar', 'EditarUsuarioController@atualizar')->name('atualizar');
});

